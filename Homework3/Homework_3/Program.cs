﻿using System;
namespace Homework_3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Pick which unit you will be converting");
            string[] convertoptions = new string[] {"Inches","Feet","Yards","Centimeters","Meters"};
            for (int i = 0; i < 5; i++)
            {
                Console.WriteLine((i + 1) + " : " + convertoptions[i]);
            }
            int userunit1 = int.Parse(Console.ReadLine());
            Console.WriteLine("How much of that unit is there?");
            double input = int.Parse(Console.ReadLine());
            Console.WriteLine("Now what will you convert it to?");
            for (int i = 0; i < 5; i++)
            {
                Console.WriteLine((i + 1) + " : " + convertoptions[i]);
            }
            int userunit2 = int.Parse(Console.ReadLine());
            if (userunit1 == 1)
            {
                Console.WriteLine(input + " Inches is equal to " + InchesIndex(input, userunit2) + "" + convertoptions[userunit2-1]);
            } else if (userunit1 == 2)
            {
                Console.WriteLine(input + " Feet is equal to " + " is equal to " + FeetIndex(input, userunit2) + " " + convertoptions[userunit2 - 1]);
            } else if (userunit1 == 3)
            {
                Console.WriteLine(input + " Yards is equal to " + convertoptions[userunit1-1] + " is equal to " + YardsIndex(input, userunit2) + " " + convertoptions[userunit2 - 1]);
            } else if (userunit1 == 4)
            {
                Console.WriteLine(input + " Centimeters is equal to " + convertoptions[userunit1 - 1] + " is equal to " + CentIndex(input, userunit2) + " " + convertoptions[userunit2 - 1]);
            } else
            {
                Console.WriteLine(input + " Meters is equal to " + convertoptions[userunit1 - 1] + " is equal to " + MetersIndex(input, userunit2) + " " + convertoptions[userunit2 - 1]);
            }
            Console.ReadLine();
        }
        static double InchesIndex(double value, int qualifier)
        {
            if (qualifier == 1)
            {
                return value;
            }
            else if (qualifier == 2)
            {
                return value / 12;
            }
            else if (qualifier == 3)
            {
                return value / 36;
            }
            else if (qualifier == 4)
            {
                return value * 2.54;
            }
            else
            {
                return value / 39.3701;
            }
        }
        static double FeetIndex(double value, int qualifier)
        {
            if (qualifier == 1)
            {
                return value * 12;
            }
            else if (qualifier == 2)
            {
                return value;
            }
            else if (qualifier == 3)
            {
                return value / 3;
            }
            else if (qualifier == 4)
            {
                return value * 30.48;
            }
            else
            {
                return value / 3.28084;
            }
        }
        static double YardsIndex(double value, int qualifier)
        {
            if (qualifier == 1)
            {
                return value * 36;
            }
            else if (qualifier == 2)
            {
                return value * 3;
            }
            else if (qualifier == 3)
            {
                return value;
            }
            else if (qualifier == 4)
            {
                return value * 91.44;
            }
            else
            {
                return value / 1.09361;
            }
        }
        static double CentIndex(double value, int qualifier)
        {
            if (qualifier == 1)
            {
                return value/2.54;
            }
            else if (qualifier == 2)
            {
                return value/30.48;
            }
            else if (qualifier == 3)
            {
                return value / 91.44;
            }
            else if (qualifier == 4)
            {
                return value;
            }
            else
            {
                return value/100;
            }
        }
        static double MetersIndex(double value, int qualifier)
        {
            if (qualifier == 1)
            {
                return value * 39.3701;
            }
            else if (qualifier == 2)
            {
                return value * 3.28084;
            }
            else if (qualifier == 3)
            {
                return value * 1.09361;
            }
            else if (qualifier == 4)
            {
                return value * 100;
            }
            else
            {
                return value;
            }
        }
    }
}