﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RandDiceRoll
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] Board = new string[9];
            int gamewon = 0;
            for (int i = 0; i < 9; i++)
            {
                if (i % 3 == 0)
                {
                    Console.WriteLine();
                }
                Board[i] = "" + (i + 1);
                if (!(i % 3 == 0))
                {
                    Console.Write("|");
                }
                Console.Write(Board[i]);
            }
            Console.WriteLine();
            while (gamewon == 0)
            {
                int playerInput = int.Parse(Console.ReadLine());
                PlayerTurn(playerInput,Board);
                EnemyTurn(Board);
                gamewon = WinCheck(Board);
            }
            if (gamewon == 1)
            {
                Console.WriteLine("You Won!");
            } else
            {
                Console.WriteLine("Robot Won!");
            }
            Console.ReadLine();
        }
        static void PlayerTurn(int playerInput, String[] Board)
        {
            Console.Clear();
            for (int i = 0; i < 9; i++)
            {
                if (i % 3 == 0)
                {
                    Console.WriteLine();
                }
                if (!(Board[i] == "X" || Board[i] == "O") && playerInput == int.Parse(Board[i]))
                {
                    Board[i] = "X";
                }
                else if (!(Board[i] == "X" || Board[i] == "O"))
                {
                    Board[i] = "" + (i + 1);
                }
                if (!(i % 3 == 0))
                {
                    Console.Write("|");
                }
                Console.Write(Board[i]);
            }
            Console.WriteLine();
        }
        static void EnemyTurn(String[] Board)
        {
            bool turnChance;
            Console.Clear();
            turnChance = true;
            for (int i = 0; i < 9; i++)
            {
                if (i % 3 == 0)
                {
                    Console.WriteLine();
                }
                if (!(Board[i] == "X" || Board[i] == "O") && turnChance)
                {
                    Board[i] = "O";
                    turnChance = false;
                }
                else if (!(Board[i] == "X" || Board[i] == "O"))
                {
                    Board[i] = "" + (i + 1);
                }
                if (!(i % 3 == 0))
                {
                    Console.Write("|");
                }
                Console.Write(Board[i]);
            }
            Console.WriteLine();
        }
        static int WinCheck(String[] Board)
        {
            int win = 0;
            for (int i = 0; i < 9; i++)
            {
                if ((i % 3 == 0))
                {
                    if (Board[i] == "X" && Board[i + 1] == "X" && Board[i + 2] == "X")
                    {
                        win = 1;
                    } 
                }
                if (i <= 3)
                {
                    if (Board[i] == "X" && Board[i + 3] == "X" && Board[i + 6] == "X")
                    {
                        win = 1;
                    }
                }
                if (i == 0)
                {
                    if (Board[i] == "X" && Board[i + 4] == "X" && Board[i + 8] == "X")
                    {
                        win = 1;
                    }
                }
                if (i == 2)
                {
                    if (Board[i] == "X" && Board[i + 2] == "X" && Board[i + 4] == "X")
                    {
                        win = 1;
                    }
                }
            }
            return win;
        }
    }
}
