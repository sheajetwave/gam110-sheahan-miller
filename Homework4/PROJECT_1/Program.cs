﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_1
{
    class Program
    {
        static Random rand = new Random();

        static int strength;
        static int dexterity;
        static int intelligence;

        static void Main(string[] args)
        {
            String[] characterSelection = {"Barbarian", "Rogue", "Alex" };
            Console.WriteLine("Select a class");
            for (int i = 0; i < characterSelection.Length; i++)
            {
                Console.WriteLine((i + 1) + " : " + characterSelection[i]);
            }
            int chosenClass = int.Parse(Console.ReadLine()) - 1;
            Console.WriteLine("The stats for your " + characterSelection[chosenClass] + " are:");
            DecideCharacter(chosenClass);
            Console.WriteLine("Strength : " + strength);
            Console.WriteLine("Dexterity : " + dexterity);
            Console.WriteLine("Intelligence : " + intelligence);
            Console.ReadLine();


        }
        static void DecideCharacter(int charClass)
        {
            if (charClass == 1)
            {
                BarbarianBuild();
            }
            else if (charClass == 2)
            {
                RogueBuild();
            }
            else if (charClass == 3)
            {
                AlexBuild();
            }
        }
        static void AlexBuild()
        {
            strength = (Roll() - 2);
            dexterity = (Roll() - 2);
            intelligence = (Roll() + 3);
        }
        static void BarbarianBuild()
        {
            strength = (Roll() + 5);
            dexterity = (Roll() - 2);
            intelligence = (Roll() - 2);
        }
        static void RogueBuild()
        {
            strength = (Roll() - 3);
            dexterity = (Roll() + 2);
            intelligence = (Roll() + 1);
        }
        static int Roll()
        {
            int[] numberList = {0,0,0,0};
            int total = 0;
            int roll = 0;
            for (int i = 0; i < 4; i++)
            {
                roll = (rand.Next(6) + 1);
                numberList[i] += roll;
                total += roll;
            }
            total -= numberList.Min();
            if (total > 18)
            {
                total = 18;
            }
            return total;
        }
    }
}