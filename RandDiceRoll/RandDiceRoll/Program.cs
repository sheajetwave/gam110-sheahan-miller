﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RandDiceRoll
{
    class Program
    {
        static Random rand = new Random();

        static void Main(string[] args)
        {
            Console.WriteLine("Roll the Dice");
            Console.WriteLine("   ");
            Console.WriteLine("How many times?");
            int rolls = int.Parse(Console.ReadLine());
            Console.WriteLine("How many sides do you want?");
            int sides = int.Parse(Console.ReadLine());
            Console.WriteLine("Total = " + Roll(rolls, sides));
            Console.ReadLine();
        }

        static int Roll(int methodrolls, int methodsides)
        {
            int total = 0;
            int roll = 0;
            for (int i = 0; i < methodrolls; i++)
            {
                roll = (rand.Next(methodsides) + 1);
                Console.WriteLine(i + " : " + roll);
                total += roll;
            }
            return total;
        }
    }
}