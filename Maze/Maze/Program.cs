﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Runtime.CompilerServices.RuntimeHelpers;

namespace RandDiceRoll
{
    class Program
    {
        public static string[,] maze = new string[30, 20];
        public static bool keyGot = false;
        static void Main(string[] args)
        {
            int[] charPlace = new int[2];
            int[] keyPlace = new int[2];
            charPlace[0] = 1;
            charPlace[1] = 1;
            keyPlace[0] = 15;
            keyPlace[1] = 10;
            bool win = false;
            bool quit = false;
            while (!win && !quit)
            {
                BuildMaze(maze, charPlace, keyPlace);
                ConsoleKey inputKey = Console.ReadKey().Key;
                if (inputKey == ConsoleKey.UpArrow && UpCheck(charPlace)) {
                    charPlace[0]--;
                }
                if (inputKey == ConsoleKey.DownArrow && DownCheck(charPlace))
                {
                    charPlace[0]++;
                }
                if (inputKey == ConsoleKey.RightArrow && RightCheck(charPlace))
                {
                    charPlace[1]++;
                }
                if (inputKey == ConsoleKey.Q)
                {
                    quit = true;
                }
                if (inputKey == ConsoleKey.LeftArrow && LeftCheck(charPlace))
                {
                    charPlace[1]--;
                }
                if (keyPlace[0] == charPlace[0] && keyPlace[1] == charPlace[1])
                {
                    keyGot = true;

                }
                if (charPlace[0] == 19 && charPlace[1] == 7)
                {
                    win = true;
                }
            }
            if (win)
            {
                Console.Clear();
                Console.WriteLine("You Won!");
                Console.ReadLine();
            }
        }

        private static bool UpCheck(int[] charPlace)
        {
            if ((maze[charPlace[1], charPlace[0] - 1] == "#") || (maze[charPlace[1], charPlace[0] - 1] == "|"))
                return false;
            else
                return true;
        }
        private static bool DownCheck(int[] charPlace)
        {
            if ((maze[charPlace[1], charPlace[0] + 1] == "#") || (maze[charPlace[1], charPlace[0] + 1] == "|"))
                return false;
            else
                return true;
        }
        private static bool RightCheck(int[] charPlace)
        {
            if ((maze[charPlace[1] + 1, charPlace[0]] == "#") || (maze[charPlace[1] + 1, charPlace[0]] == "|"))
                return false;
            else
                return true;
        }
        private static bool LeftCheck(int[] charPlace)
        {
            if ((maze[charPlace[1] - 1, charPlace[0]] == "#") || (maze[charPlace[1] - 1, charPlace[0]] == "|"))
                return false;
            else
                return true;
        }
        private static void BuildMaze(string[,] maze, int[] charPlace, int[] keyPlace)
        {
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.DarkRed;
            for (int y = 0; y < maze.GetLength(1); y++)
            {
                for (int x = 0; x < maze.GetLength(0); x++)
                {
                    if (charPlace[0] == y && charPlace[1] == x)
                    {
                        maze[x, y] = "o";

                    } else if (keyPlace[0] == y && keyPlace[1] == x && !keyGot)
                    {
                        maze[x, y] = "+";
                    } else if (x == 10 && y == 3 && !keyGot) {
                        maze[x, y] = "|";
                    } else if (((x == 20) && (y > 3) && (y < 18)) || ((x == 1) && (y > 0) && (y < 10)) || ((y == 3) && (x > 0) && (x < 21)) || ((y == 3) && (x > 0) && (x < 8)) || ((x == 7) && (y > 0) && (y < 3)) || ((y == 8) &&
                      (x > 1) && (x < 11)) || ((x == 10) && (y > 8) && (y < 15)) || ((y == 6) && (x > 10) && (x < 20)) || ((x == 7) && (y > 10) && (y < 20)) || ((y == 18) && (x > 1) && (x < 25))) 
                    {
                        maze[x, y] = " ";
                    } else
                    {
                        maze[x, y] = "#";
                    }
                    Console.Write(maze[x, y]);
                }
                Console.WriteLine("");
            }

                
        }
    }
}