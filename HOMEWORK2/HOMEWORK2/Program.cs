﻿using System;
using Microsoft.VisualBasic.FileIO;

namespace HOMEWORK2
{
    class Program
    {
        static void Main(string[] args)
        {
            bool conversion = true;
            while (conversion == true)
            {
                Greeting();
                string input = Console.ReadLine();
                float option;
                bool inputTryparse = float.TryParse(input, out option);
                if (inputTryparse == true && option < 4 && option > 0)
                {
                    UnitConverter(option);
                    Console.WriteLine("Would you like to keep converting? Yes or no");
                    string yesNo = Console.ReadLine();
                    if (yesNo.ToLower() == "yes")
                    {
                        conversion = true;
                    } else
                    {
                        conversion = false;
                    }
                }
                else
                {
                    Console.WriteLine("Please enter in a number corresponding to the unit conversion");
                }
            }
        }
        static void Greeting()
        {
            Console.WriteLine("Hello!");
            Console.WriteLine("1. Inches to feet");
            Console.WriteLine("2. Yards to inches");
            Console.WriteLine("3. Feet to yards");
        }
        static void UnitConverter(float x)
        {
            if (x == 1)
            {
                Console.WriteLine("How many inches?");
            }
            else if (x == 2)
            {
                Console.WriteLine("How many Yards?");
            }
            else if (x == 3)
            {
                Console.WriteLine("How many Feet?");
            }
            int amount;
            bool amountCheck = int.TryParse(Console.ReadLine(), out amount);
            if (x == 1)
            {
                Console.WriteLine(amount + " inches is " + amount * 0.0833333333333333 + " feet");
            }
            else if (x == 2)
            {
                Console.WriteLine(amount + " yards is " + amount * 36 + " inches");
            }
            else if (x == 3)
            {
                Console.WriteLine(amount + " feet is " + amount * 0.33333333333333333333 + " yards");
            }
        }
    }
}