﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Runtime.CompilerServices.RuntimeHelpers;

namespace HaroldsQuest
{
    struct BulletType {
        public string name;
        public int[] location;
        public int speed;
        public int damage;
    }
    class Program
    {
        public static string[,] maze = new string[30, 20];
        public static string direction = "right";
        public static bool keyGot = false;
        public static string[] inventory = new string[3];
        public static bool[] collectible = new bool[] { false, false, false };
        static void Main(string[] args)
        {
            BulletType[] bulletTypes = new BulletType[3];

            string[] BulletNames = new string[3] { "Basic", "Quick", "Heavy" };
            int[] BulletSpeed = new int[3] {5, 15, 3};
            int[] BulletDamage = new int[3] {3, 2, 20};
            int[,] BulletLocations = new int[2, 3] {{ 3, 4, 8 },{ 7, 10, 6 }};
            for (int i = 0; i < bulletTypes.Length; i++)
            {
                bulletTypes[i].location = new int[2];
                bulletTypes[i].name = BulletNames[i];
                bulletTypes[i].speed = BulletSpeed[i];
                bulletTypes[i].damage = BulletDamage[i];
                bulletTypes[i].location[0] = BulletLocations[0, i];
                bulletTypes[i].location[1] = BulletLocations[1, i];
            }
            List <string> Inventory = new List<string>();
            int[] charPlace = new int[2];
            int[] keyPlace = new int[2];
            charPlace[0] = 1;
            charPlace[1] = 1;
            keyPlace[0] = 15;
            keyPlace[1] = 10;
            bool win = false;
            bool quit = false;
            bool dropable = true;
            while (!win && !quit)
            {
                BuildMaze(maze, charPlace, keyPlace, Inventory, bulletTypes);
                ConsoleKey inputKey = Console.ReadKey().Key;
                if (inputKey == ConsoleKey.UpArrow && UpCheck(charPlace))
                {
                    charPlace[0]--;
                }
                if (inputKey == ConsoleKey.DownArrow && DownCheck(charPlace))
                {
                    charPlace[0]++;
                }
                if (inputKey == ConsoleKey.RightArrow && RightCheck(charPlace))
                {
                    charPlace[1]++;
                }
                if (inputKey == ConsoleKey.Q)
                {
                    quit = true;
                }
                if (inputKey == ConsoleKey.LeftArrow && LeftCheck(charPlace))
                {
                    charPlace[1]--;
                }
                if (inputKey == ConsoleKey.Spacebar)
                {
                        for (int i = 0; i < bulletTypes.Length; i++)
                        {
                            if (bulletTypes[i].location[1] == charPlace[1] && bulletTypes[i].location[0] == charPlace[0] && !collectible[i])
                            {
                            Inventory.Add(bulletTypes[i].name + " [Speed:" + bulletTypes[i].speed + " Strength:" + bulletTypes[i].damage + "]");
                                collectible[i] = true;
                                dropable = false;
                            } 
                        }
                }
                if (charPlace[0] == 19 && charPlace[1] == 7)
                {
                    win = true;
                }
            }
            if (win)
            {
                Console.Clear();
                Console.WriteLine("You Won!");
                Console.ReadLine();
            }
        }

        private static bool UpCheck(int[] charPlace)
        {
            if ((maze[charPlace[1], charPlace[0] - 1] == "#") || (maze[charPlace[1], charPlace[0] - 1] == "|"))
                return false;
            else
            {
                return true;
                direction = "up";
            }
        }
        private static bool DownCheck(int[] charPlace)
        {
            if ((maze[charPlace[1], charPlace[0] + 1] == "#") || (maze[charPlace[1], charPlace[0] + 1] == "|"))
                return false;
            else
            {
                return true;
                direction = "down";
            }
        }
        private static bool RightCheck(int[] charPlace)
        {
            if ((maze[charPlace[1] + 1, charPlace[0]] == "#") || (maze[charPlace[1] + 1, charPlace[0]] == "|"))
                return false;
            else
            {
                return true;
                direction = "right";
            }
        }
        private static bool LeftCheck(int[] charPlace)
        {
            if ((maze[charPlace[1] - 1, charPlace[0]] == "#") || (maze[charPlace[1] - 1, charPlace[0]] == "|"))
                return false;
            else
            {
                return true;
            }
        }
        private static void BuildMaze(string[,] maze, int[] charPlace, int[] keyPlace, List<string> Pickups, BulletType[] Pickupinfo)
        {
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.DarkRed;
            for (int y = 0; y < maze.GetLength(1); y++)
            {
                for (int x = 0; x < maze.GetLength(0); x++)
                {
                    if (charPlace[0] == y && charPlace[1] == x)
                    {
                        maze[x, y] = "O";

                    } else if ((y == Pickupinfo[0].location[0] && x == Pickupinfo[0].location[1] && !collectible[0]) || (y == Pickupinfo[1].location[0] && x == Pickupinfo[1].location[1] && !collectible[1]) || (y == Pickupinfo[2].location[0] && x == Pickupinfo[2].location[1]) && !collectible[2])
                    {
                        maze[x, y] = "!";
                    } else if (x == 29 && y == 3)
                    {
                        if (!keyGot)
                            maze[x, y] = "|";
                        else
                            maze[x, y] = " ";
                    }
                    else if (x > 0 && x < 29 && y > 0 && y < 19)
                    {
                        maze[x, y] = " ";
                    }
                    else
                    {
                        maze[x, y] = "#";
                    }
                    Console.Write(maze[x, y]);
                }
                Console.WriteLine("");
            }
            Console.WriteLine("Move With Arrow Keys and PickUp with Space Bar");
            Console.WriteLine("");
            Console.Write("Inventory :");
            foreach (var item in Pickups)
            {
                Console.WriteLine(item);
            }
        }
    }
}